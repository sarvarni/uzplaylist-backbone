<?php 
    header( 'Access-Control-Allow-Origin: *'); 
    require $_SERVER['DOCUMENT_ROOT'] . '/api/Slim/Slim.php';
    \Slim\Slim::registerAutoloader();

    $app = new \Slim\Slim();
 
	$app->get('/song', 'getSongs');
	$app->get('/song/:id', 'getSong');
	$app->get('/song/search/:query', 'findByTitle');
	$app->post('/song', 'addSong');
	$app->put('/song/:id', 'updateViews');
	$app->get('/artist', 'getArtists');
	$app->post('/upload', 'post_upload');
	$app->post('/contact', 'contact');
	$app->delete('/song/:id', 'deleteSong');
	 
	$app->run();
	 
	function getSongs() {
	    $sql = "Select song.ID, song.title, artist.id as artistID, artist.name as artistName, song.fileName FROM song, artist WHERE song.public = 1 and song.artist = artist.ID ORDER BY RAND()";
	    try {
	        $db = getConnection();
	        $stmt = $db->query($sql);
	        $wines = $stmt->fetchAll(PDO::FETCH_OBJ);
	        $db = null;
	        echo json_encode($wines);
	    } catch(PDOException $e) {
	        echo '{"error":{"text":'. $e->getMessage() .'}}';
	    }
	}
	 
	function getSong($id) {
	    $sql = "SELECT * FROM wine WHERE id=:id";
	    try {
	        $db = getConnection();
	        $stmt = $db->prepare($sql);
	        $stmt->bindParam("id", $id);
	        $stmt->execute();
	        $wine = $stmt->fetchObject();
	        $db = null;
	        echo json_encode($wine);
	    } catch(PDOException $e) {
	        echo '{"error":{"text":'. $e->getMessage() .'}}';
	    }
	}
	 
	/*function addSong() {
	    $request = Slim::getInstance()->request();
	    $wine = json_decode($request->getBody());
	    $sql = "INSERT INTO wine (name, grapes, country, region, year, description) VALUES (:name, :grapes, :country, :region, :year, :description)";
	    try {
	        $db = getConnection();
	        $stmt = $db->prepare($sql);
	        $stmt->bindParam("name", $wine->name);
	        $stmt->bindParam("grapes", $wine->grapes);
	        $stmt->bindParam("country", $wine->country);
	        $stmt->bindParam("region", $wine->region);
	        $stmt->bindParam("year", $wine->year);
	        $stmt->bindParam("description", $wine->description);
	        $stmt->execute();
	        $wine->id = $db->lastInsertId();
	        $db = null;
	        echo json_encode($wine);
	    } catch(PDOException $e) {
	        echo '{"error":{"text":'. $e->getMessage() .'}}';
	    }
	}*/

	function addSong() {
	    $request = \Slim\Slim::getInstance()->request();
	    $song= json_decode($request->getBody());
	    $sql = "INSERT INTO song (title, artist, fileName, public) VALUES (:title, :artist, :fileName, '1')";
	    try {
	        $db = getConnection();
	        $stmt = $db->prepare($sql);
	        $stmt->bindParam("title", $song->title);
	        $stmt->bindParam("artist", $song->artistID);
	        $stmt->bindParam("fileName", $song->fileName);
	        $stmt->execute();
	        $song->id = $db->lastInsertId();
	        $db = null;
	        echo json_encode($song);
	    } catch(PDOException $e) {
	        echo '{"error":{"text":'. $e->getMessage() .'}}';
	    }
	}
	 
	function updateViews($id) {
	    $request = Slim::getInstance()->request();
	    $body = $request->getBody();
	    $song = json_decode($body);
	    $sql = "UPDATE song SET views = views + 1 WHERE ID=:id";
	    try {
	        $db = getConnection();
	        $stmt = $db->prepare($sql);
	        $stmt->bindParam("views", $song->views);
	        $stmt->execute();
	        $db = null;
	        echo json_encode($song);
	    } catch(PDOException $e) {
	        echo '{"error":{"text":'. $e->getMessage() .'}}';
	    }
	}
	 
	function deleteSong($id) {
	    $sql = "DELETE FROM wine WHERE id=:id";
	    try {
	        $db = getConnection();
	        $stmt = $db->prepare($sql);
	        $stmt->bindParam("id", $id);
	        $stmt->execute();
	        $db = null;
	    } catch(PDOException $e) {
	        echo '{"error":{"text":'. $e->getMessage() .'}}';
	    }
	}

	function getArtists() {
	    $sql = "Select id, name FROM artist ORDER BY name";
	    try {
	        $db = getConnection();
	        $stmt = $db->query($sql);
	        $wines = $stmt->fetchAll(PDO::FETCH_OBJ);
	        $db = null;
	        echo json_encode($wines);
	    } catch(PDOException $e) {
	        echo '{"error":{"text":'. $e->getMessage() .'}}';
	    }
	}
	 
	function findByTitle($query) {
	    $sql = "Select ID, title, artist, fileName FROM song WHERE public = 1 MATCH(title) AGAINST('+" . $query . "') ORDER BY title";
	    try {
	        $db = getConnection();
	        $stmt = $db->prepare($sql);
	        $query = "%".$query."%";
	        $stmt->bindParam("query", $query);
	        $stmt->execute();
	        $wines = $stmt->fetchAll(PDO::FETCH_OBJ);
	        $db = null;
	        echo json_encode($wines);
	    } catch(PDOException $e) {
	        echo '{"error":{"text":'. $e->getMessage() .'}}';
	    }
	}

	function post_upload() {

		$ds = '/';
		$storeFolder = 'files';
 
		if (!empty($_FILES)) {
		     
		    $tempFile = $_FILES['file']['tmp_name'];
		      
		    $targetPath = $_SERVER['DOCUMENT_ROOT'] . $ds . $storeFolder . $ds;
		     
		    //$targetFile =  $targetPath . $_FILES['file']['name'];
		    $ext = '.' . pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

		    $targetName = hash('adler32', (time().time()));

		    $targetFile =  $targetPath . $targetName . $ext;
		 
		    $upload_success = move_uploaded_file($tempFile, $targetFile);
		    if($upload_success) {
	        	//echo $targetName;
	        	echo '{"success":{"fileName":"' . $targetName . '"}}';
	        } else {
	        	echo '{"error":{"text": "Uploading ' . $_FILES['file']['name'] . ' was not successful. Please try again later."}}';
	        }   
		} else {
			echo '{"error":{"text": "No files sent."}}';
		}
	}

	function contact() {
		$request = \Slim\Slim::getInstance()->request();
	    $contact = json_decode($request->getBody());

	    if(!empty($contact->email) && !empty($contact->msg)) {

	    	if (filter_var($contact->email, FILTER_VALIDATE_EMAIL)) {

				//Email information
				$admin_email = "support@uzplaylist.com";
				$email = $contact->email;
				$subject = 'UzPlaylist Contact - ' . $email;
				$msg = $contact->msg;
			  
			  	//send email
			  	if(mail($admin_email, $subject, $msg, "From:" . $email)) {
			  		echo '{"success":{"text": "Successfully sent."}}';
			  	} else {
			  		echo '{"error":{"text": "Unable to send."}}';
			  	}

			} else {
				echo '{"error":{"text": "Invalid email address."}}';
			}
	    } else {
	    	echo '{"error":{"text": "Required fields were null."}}';
	    }


	}
	 
	function getConnection() {
		$local = array(
		    '127.0.0.1',
		    '::1'
		);

		if(in_array($_SERVER['REMOTE_ADDR'], $local)){
			
		    // Localhost settings
		    $dbhost="localhost";
		    $dbuser="root";
		    $dbpass="root";
		    $dbname="uzplaylist";
		} else {

			// Production Settings
			$dbhost="localhost";
		    $dbuser="tashbaqa_1";
		    $dbpass="727827";
		    $dbname="tashbaqa_uzplaylist";
		}
	    
	    $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
	    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    return $dbh;
	}
 
?>