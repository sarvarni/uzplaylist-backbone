define(['underscore', 'backbone', 'models/modelSong'], function(_, Backbone, modelSong) {
    'use strict';

    var collectionSongs = Backbone.Collection.extend({
        model: modelSong,
        url: 'api/v1.0/song',

        search: function(letters) {
            if (letters === '') {
                return this;
            }

            var pattern = new RegExp(letters, 'gi');

            return _(
                this.filter(function(data) {
                    var testTitle = pattern.test(data.get('title'));
                    var testArtist = pattern.test(data.get('artistName'));
                    return (testTitle) ? testTitle : testArtist;
                })
            );
        }
    });

    return collectionSongs;
});