define(['underscore', 'backbone', 'models/modelSong'], function(_, Backbone, modelSong) {
    'use strict';

    var userPlaylist = Backbone.Collection.extend({
        model: modelSong,
        localStorageNameSpace: 'userPlaylist',

        initialize: function() {
            this.fetch();
        },

        search: function(letters) {
            if (letters === '') {
                return this;
            }

            var pattern = new RegExp(letters, 'gi');

            return _(
                this.filter(function(data) {
                    var testTitle = pattern.test(data.get('title'));
                    var testArtist = pattern.test(data.get('artistName'));
                    return (testTitle) ? testTitle : testArtist;
                })
            );
        },

        fetch: function() {
            // Reset now playing to false
            if(localStorage.getItem(this.localStorageNameSpace) !== null) {
            	this.set(JSON.parse(localStorage.getItem(this.localStorageNameSpace).replace('"nowPlaying":true', '"nowPlaying":false')));
        	}
        },

        save: function(attributes) {
            localStorage.setItem(this.localStorageNameSpace, JSON.stringify(this.toJSON()));
        },

        destroy: function(options) {
            localStorage.removeItem(this.localStorageNameSpace);
        }
    });

    return userPlaylist;
});