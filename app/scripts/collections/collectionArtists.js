define(['backbone', 'models/modelArtist'], function (Backbone, modelArtist) {
    'use strict';

	var collectionArtists = Backbone.Collection.extend({
		model: modelArtist,
        url: 'api/v1.0/artist'
    });

    return collectionArtists;
});