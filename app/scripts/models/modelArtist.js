define(['backbone'],
    function (Backbone) {
        'use strict';

        var modelArtist = Backbone.Model.extend({
            defaults: {
                'ID': '',
                'title': '',
                'numSongs': ''
            }
        });

        return modelArtist;
    });
        
        