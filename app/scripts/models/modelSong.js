define(['backbone'], function(Backbone) {
    'use strict';

    var modelSong = Backbone.Model.extend({
        urlRoot: 'api/v1.0/song',
        defaults: function() {
            return {
                'ID': '',
                'title': '',
                'artistID': '',
                'artistName': '',
                'fileName': '',
                'nowPlaying': false
            };
        },

        validate: function(attrs) {
            if (!attrs.title) {
                return 'Song title is required.';
            }
            if (!attrs.artistID) {
                return 'Song artist id is required.';
            }
            if (!attrs.artistName) {
                return 'Song artist name is required.';
            }
            if (!attrs.fileName) {
                return 'Song filename is required.';
            }
        }
    });

    return modelSong;
});