define(['jquery'],
    function($) {
        'use strict';

        var Helper = {
            openQueue: function() {
                $(function() {
                    $('footer .navbar-fixed-bottom').animate({
                        'height': $(window).height() - 51
                    }, 300).addClass('playlist-open');
                });

            },

            closeQueue: function() {
                $(function() {
                    $('footer .navbar-fixed-bottom').animate({
                        'height': '63px'
                    }, 300).removeClass('playlist-open');
                });
            }
        };

        return Helper;
    }
);