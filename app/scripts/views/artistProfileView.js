define(['jquery', 'underscore', 'backbone', 'requirejstext!templates/artistProfile.html', 'requirejstext!locale/en_us/search.json'],
    function($, _, Backbone, templates, content) {
        'use strict';

        var artistProfileView = Backbone.View.extend({
            el: '.results',

            render: function(options) {
                var that = this;
                if (options !== undefined) {
                    console.log(options.query);
                }

                var artists = new Artists();

                artists.fetch({
                    success: function(artists) {

                        var template = _.template(templates, {
                            artists: artists.models,
                            content: JSON.parse(content)
                        });

                        that.$el.html(template);
                    },

                    error: function() {
                        $('.results').html('error');
                    }
                });

                // Maintain chainability
                return this;
            }
        });

        return artistProfileView;
    });