define(['jquery', 'underscore', 'backbone', 'requirejstext!templates/results.html', 'views/songView', 'collections/collectionSongs'],
    function($, _, Backbone, template, songView, collectionSongs) {
        'use strict';

        var resultsView = Backbone.View.extend({
            filterCollection: new collectionSongs(),

            initialize: function(options) {
                this.userplaylist = options.userplaylist;
                this.collectionSongs = options.collectionSongs;

                this.listenTo(this.collectionSongs, 'sync', this.render);
            },

            events: {
                'keyup #search': 'search',
                'click #btn-play-all': 'addToPlaylist'
            },

            render: function() {

                this.template = _.template(template);

                this.$el.html(this.template);

                if (this.collectionSongs.size() === 0) {
                    this.$el.find('#results').html('Loading...');
                } else {
                    this.renderList(this.collectionSongs);
                }

                // Maintain chainability
                return this;
            },

            search: function(e) {
                if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode === 8)) {
                    var letters = this.$el.find('#search').val();
                    var self = this;

                    if (letters.length > 0) {
                        //Scroll to top
                        $('html, body').animate({
                            scrollTop: 0
                        }, 300);

                        this.filterCollection.reset();

                        var filtered = this.collectionSongs.search(letters);

                        if (filtered.toArray().length > 0) {
                            filtered.each(function(modal) {
                                self.filterCollection.add(modal);
                            });

                            this.renderList(this.filterCollection);
                            this.$el.find('#btn-play-all').prop('disabled', false);
                        } else {
                            this.$el.find('#results').html('No results found.');
                            this.$el.find('#btn-play-all').prop('disabled', true);
                        }
                    } else {
                        if (this.collectionSongs.length > 0) {
                            this.renderList(this.collectionSongs);
                            this.$el.find('#btn-play-all').prop('disabled', false);
                        } else {
                            this.$el.find('#results').html('No results found.');
                            this.$el.find('#btn-play-all').prop('disabled', true);
                        }
                    }
                }
            },

            renderList: function(item) {
                var self = this;
                this.$el.find('#results').html('');

                _.each(item.toArray(), function(song) {

                    var childView = new songView({
                        model: song,
                        userplaylist: self.userplaylist
                    });

                    self.$el.find('#results').append(childView.render().$el);
                });
            },

            addToPlaylist: function() {
                var self = this,
                    filtered = (this.$el.find('#search').val().length > 0) ? true : false;

                if (filtered) {
                    _.each(this.filterCollection.toArray(), function(model) {
                        if (self.userplaylist.where({
                            'ID': model.get('ID')
                        }).length === 0) {

                            self.userplaylist.add(model);
                            self.userplaylist.save();

                        }
                    });
                } else {
                    _.each(this.collectionSongs.toArray(), function(model) {
                        if (self.userplaylist.where({
                            'ID': model.get('ID')
                        }).length === 0) {

                            self.userplaylist.add(model);
                            self.userplaylist.save();

                        }
                    });
                }

                // Play from beginning
                window.uzPlaylist.play(0);
            }
        });

        return resultsView;
    });