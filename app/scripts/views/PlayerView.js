define(['jquery', 'underscore', 'backbone', 'jplaylist', 'requirejstext!templates/footer.player.html'],
    function($, _, Backbone, jplaylist, template) {
        'use strict';

        var playerView = Backbone.View.extend({
            el: 'footer',

            initialize: function(options) {

                _.bindAll(this, 'render');

                this.userplaylist = options.userplaylist;
                this.collectionSongs = options.collectionSongs;

                var prevSong = 0,
                    self = this;

                this.listenTo(this.userplaylist, 'add', this.addToPlaylist);
                this.listenTo(this.userplaylist, 'remove', this.removeFromPlaylist);

                this.cssSelector = {
                    jPlayer: '#uzplaylist',
                    cssSelectorAncestor: '#uzplaylist_container'
                };

                this.playlist = [];

                this.options = {
                    supplied: 'mp3',
                    swfPath: 'bower_components/jplayer/dist/jplayer',
                    enableRemoveControls: 'true',
                    fullScreen: 'true',
                    cssSelector: {
                        play: '.fa-play',
                        previous: '.fa-step-backward',
                        next: '.fa-step-forward',
                        pause: '.fa-pause',
                        stop: '.fa-stop',
                        seekBar: '.jp-seek-bar',
                        playBar: '.jp-play-bar',
                        mute: '.jp-mute',
                        unmute: '.jp-unmute',
                        volumeBar: '.jp-volume-bar',
                        volumeBarValue: '.jp-volume-bar-value',
                        volumeMax: '.jp-volume-max',
                        playbackRateBar: '.jp-playback-rate-bar',
                        playbackRateBarValue: '.jp-playback-rate-bar-value',
                        currentTime: '.jp-current-time',
                        duration: '.jp-duration',
                        title: '.jp-title', // .jp-title
                        fullScreen: '.jp-full-screen',
                        restoreScreen: '.jp-restore-screen',
                        repeat: '.jp-repeat',
                        repeatOff: '.jp-repeat-off',
                        gui: '.jp-gui',
                        noSolution: '.jp-no-solution'
                    },
                    play: function(event) {
                        if (prevSong > 0) {
                            self.removeNowPlaying(prevSong);
                        }
                        prevSong = event.jPlayer.status.media.id;
                        self.addNowPlaying(prevSong);
                    },
                    pause: function(event) {
                        self.removeNowPlaying(event.jPlayer.status.media.id);
                    },
                    ended: function(event) {
                        self.removeNowPlaying(event.jPlayer.status.media.id);
                    },
                    loadeddata: function(event) {
                        self.setArtist(event.jPlayer.status.media.artist);
                    }
                };

                this.render();
            },

            events: {
                'click .fa-step-backward': 'methodPrev',
                'click .fa-step-forward': 'methodNext'
            },

            render: function() {
                var self = this;
                this.template = _.template(template);

                this.$el.html(this.template);

                window.uzPlaylist = new jPlayerPlaylist(this.cssSelector, this.playlist, this.options);

                if (this.userplaylist.length > 0) {
                    _.each(this.userplaylist.toArray(), function(model) {
                        self.addToPlaylist(model);
                    });
                }

                // Maintain chainability
                return this;
            },

            removeNowPlaying: function(id) {
                var collectionmodel = this.collectionSongs.findWhere({
                    'ID': id
                });

                var playlistmodel = this.userplaylist.findWhere({
                    'ID': id
                });


                collectionmodel.set({
                    'nowPlaying': false
                });

                if (playlistmodel !== undefined) {
                    playlistmodel.set({
                        'nowPlaying': false
                    });
                }

                // RESAVE
                this.collectionSongs.set(collectionmodel, {
                    remove: false
                });

                if (playlistmodel !== undefined) {
                    this.userplaylist.set(playlistmodel, {
                        remove: false
                    });
                }
            },

            addNowPlaying: function(id) {
                var collectionmodel = this.collectionSongs.findWhere({
                    'ID': id
                });

                var playlistmodel = this.userplaylist.findWhere({
                    'ID': id
                });


                collectionmodel.set({
                    'nowPlaying': true
                });

                playlistmodel.set({
                    'nowPlaying': true
                });

                // RESAVE
                this.collectionSongs.set(collectionmodel, {
                    remove: false
                });

                this.userplaylist.set(playlistmodel, {
                    remove: false
                });
            },

            addToPlaylist: function(model) {
                window.uzPlaylist.add({
                    id: model.get('ID'),
                    title: model.get('title'),
                    artist: model.get('artistName'),
                    mp3: 'files/' + model.get('fileName') + '.mp3'
                });
            },

            removeFromPlaylist: function(model) {
                var pos = window.uzPlaylist.playlist.map(function(e) {
                    return e.id;
                }).indexOf(model.get('ID'));

                var isPlaying = !$('#uzplaylist').data().jPlayer.status.paused;

                // if the current song being played is removed,
                // skip to the next song if possible
                if (window.uzPlaylist.current === pos && isPlaying) {
                    window.uzPlaylist.pause();
                }

                // Remove from jplaylist player array
                window.uzPlaylist.playlist.splice(pos, 1);

                if (window.uzPlaylist.playlist.length > 0 && isPlaying && window.uzPlaylist.current === pos) {
                    window.uzPlaylist.play(window.uzPlaylist.current);
                }

                if (window.uzPlaylist.playlist.length === 0) {
                    // Update text with current playing song
                    this.$el.find('.jp-title').text('Select a song to play');
                    this.$el.find('.jp-artist').text('');
                    $('#uzplaylist').jPlayer("clearMedia");
                }
            },

            methodPrev: function(e) {
                window.uzPlaylist.previous();
                e.preventDefault();
            },

            methodNext: function(e) {
                window.uzPlaylist.next();
                e.preventDefault();
            },

            methodPlay: function(e) {
                window.uzPlaylist.play();
                e.preventDefault();
            },

            setArtist: function(artist) {
                // Update text with current playing song
                this.$el.find('.jp-artist').text(artist);
            }
        });

        return playerView;
    });