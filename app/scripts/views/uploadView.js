define(['jquery', 'underscore', 'backbone', 'dropzone', 'requirejstext!templates/upload.html', 'requirejstext!templates/uploadItem.html', 'collections/collectionArtists', 'models/modelSong'],
    function($, _, Backbone, Dropzone, template, templatePreview, collectionArtists, modelSong) {
        'use strict';

        var uploadView = Backbone.View.extend({
            collectionArtists: new collectionArtists(),

            initialize: function(options) {
                this.collectionSongs = options.collectionSongs;
            },

            events: {
                'click [data-target="expand-collapse"]': 'expandCollapse',
                'submit #form-file-info': 'validateForm'
            },

            render: function() {

                this.template = _.template(template);

                this.$el.html(this.template);

                var self = this;

                this.collectionArtists.fetch({
                    success: function(item) {
                        var artists = [];
                        _.each(item.toArray(), function(model) {
                            artists.push(model);
                        });

                        self.templatePreview = _.template(templatePreview, {
                            artists: artists
                        });

                        var myDropzone = new Dropzone('.dropzone', {
                            url: 'api/v1.0/upload',
                            maxFilesize: 10,
                            previewsContainer: '.dropzone-previews',
                            clickable: '#btn-upload',
                            previewTemplate: self.templatePreview,
                            //previewTemplate: ''
                            success: function(file, response) {
                                // if json returns the success object
                                if (typeof JSON.parse(response).success === 'object') {
                                    $(file.previewTemplate).find('#file-id').val(JSON.parse(response).success.fileName);
                                    $(file.previewTemplate).find('[data-target="expand-collapse"]').click();
                                } else {
                                    // Json did not return success object, throw an error
                                    // Remove file
                                    myDropzone.removeFile(file);

                                    // Show alert msg
                                    var msg = JSON.parse(response).error.text;
                                    self.$el.find('.alert').removeClass('hidden').focus().find('.error-msg').text(msg);
                                }

                            },
                            acceptedFiles: '.mp3'
                        });
                    },
                    error: function() {
                        console.log('Cannot retrive models from server');
                    }
                });

                // Disable auto discover for all elements:
                Dropzone.autoDiscover = false;

                // Maintain chainability
                return this;
            },

            expandCollapse: function(e) {
                var expanded = ($(e.currentTarget).attr('aria-expanded') === 'true') ? true : false;

                $(e.currentTarget)
                    .html(function() {
                        var replaceFrom = (!expanded) ? 'glyphicon-chevron-down' : 'glyphicon-chevron-right';
                        var replaceTo = (expanded) ? 'glyphicon-chevron-down' : 'glyphicon-chevron-right';

                        return $(this).html().replace(replaceFrom, replaceTo);
                    })
                    .attr('aria-expanded', !expanded);

                if (expanded) {
                    $(e.currentTarget).parents('.file-item-preview').next('[data-section=' + $(e.currentTarget).attr('data-target') + ']').addClass('hidden').attr('aria-hidden', expanded);
                } else {
                    $(e.currentTarget).parents('.file-item-preview').next('[data-section=' + $(e.currentTarget).attr('data-target') + ']').removeClass('hidden').attr('aria-hidden', !expanded);
                }

                e.preventDefault();
            },

            validateForm: function(e) {
                // Create a new model for the song
                var song = new modelSong();

                var fileID = $(e.currentTarget).find('#file-id').val(),
                    artistID = parseInt($(e.currentTarget).find('#file-artist option:selected').val()),
                    artistName = $(e.currentTarget).find('#file-artist option:selected').text(),
                    title = $(e.currentTarget).find('#file-title').val();

                if (artistID === 0) {
                    $(e.currentTarget).find('#file-artist').parent('.form-group').addClass('has-error');
                } else {
                    $(e.currentTarget).find('#file-artist').parent('.form-group').removeClass('has-error');
                }

                if (title.length === 0) {
                    $(e.currentTarget).find('#file-title').parent('.form-group').addClass('has-error');
                } else {
                    $(e.currentTarget).find('#file-title').parent('.form-group').removeClass('has-error');
                }

                if (artistID !== 0 && title.length !== 0) {
                    // Save to model
                    song.set({
                        title: title,
                        artistID: artistID,
                        artistName: artistName,
                        fileName: fileID
                    }, {
                        validate: true
                    });

                    // If model is valid, save to server
                    if (song.isValid()) {
                        this.collectionSongs.create(song, {
                            success: function() {
                                $(e.currentTarget).parent('.file-item-info').prev('.file-item-preview').find('.file-success').removeClass('hidden');

                                $(e.currentTarget).parent('.file-item-info').prev('.file-item-preview').find('[data-target="expand-collapse"]').click();
                                $(e.currentTarget).parent('.file-item-info').prev('.file-item-preview').find('.progress, .file-remove, .file-info-toggle').addClass('hidden');
                            },
                            error: function() {
                                $(e.currentTarget).parent('.file-item-info').prev('.file-item-preview').find('.file-danger').removeClass('hidden');
                            }
                        });
                    } else {
                        alert('Some fields were not valid in the backend. Please contact us at support@uzplaylist.com.');
                    }

                } else {
                    // Show alert msg
                    $(e.currentTarget).find('.alert').removeClass('hidden').focus().find('.error-msg');

                    // Focus to the first error
                    $(e.currentTarget).find('.has-error:first').find('input, select').focus();
                }

                e.preventDefault();
            }
        });

        return uploadView;
    });