define(['jquery', 'underscore', 'backbone', 'requirejstext!templates/modelSong.html', 'models/modelSong'],
    function($, _, Backbone, template, modelSong) {
        'use strict';

        var songView = Backbone.View.extend({
            el: '',
            model: modelSong,
            tagName: 'li',
            className: 'list-group-item',
            attributes: function() {
                return {
                    'data-songid': this.model.get('ID'),
                    'data-filename': this.model.get('fileName')
                };
            },

            events: {
                'click [data-play]': 'addNplay',
                'click #btn-queue': 'togglePlaylist'
            },

            initialize: function(options) {
                this.template = _.template(template);
                this.userplaylist = this.options.userplaylist;

                this.listenTo(this.userplaylist, 'add', this.changeToRemPlaylist);
                this.listenTo(this.userplaylist, 'remove', this.changeToAddPlaylist);
                this.listenTo(this.model, 'change', this.nowPlaying);
            },

            render: function() {
                this.$el.html(
                    this.template({
                        file: this.model.toJSON(),
                        playlist: this.userplaylist
                    })
                );

                // Check whether the song is currently being played
                this.nowPlaying(this.model);

                // Maintain chainability
                return this;
            },

            determineTogglePlaylist: function(model) {

                if (model.get('ID') === this.model.get('ID')) {
                    this.nowPlaying(model);
                }
            },

            togglePlaylist: function(e) {
                if (this.userplaylist.where({
                    ID: this.model.get('ID')
                }).length > 0) {

                    this.userplaylist.remove(
                        this.userplaylist.findWhere({
                            ID: this.model.get('ID')
                        }).cid
                    );

                    this.userplaylist.save();
                } else {
                    this.userplaylist.add(this.model);
                    this.userplaylist.save();
                }
                e.preventDefault();
                e.stopPropagation();
            },

            changeToAddPlaylist: function(model) {
                var title = 'Add to Queue',
                    iconFrom = 'minus',
                    iconTo = 'plus';

                // Check if the current model equals to the model removed
                if (model.get('ID') === this.model.get('ID')) {
                    this.$el.find('#btn-queue').attr('title', title).html(function() {
                        return $(this).html().replace(iconFrom, iconTo);
                    });
                }
            },

            changeToRemPlaylist: function(model) {
                var title = 'Remove from Queue',
                    iconFrom = 'plus',
                    iconTo = 'minus';

                // Check if the current model equals to the model added
                if (model.get('ID') === this.model.get('ID')) {
                    this.$el.find('#btn-queue').attr('title', title).html(function() {
                        return $(this).html().replace(iconFrom, iconTo);
                    });
                }
            },

            nowPlaying: function(model) {
                if (model.get('nowPlaying') === true) {
                    this.$el.addClass('list-group-item-success');
                } else {
                    this.$el.removeClass('list-group-item-success');
                }
            },

            addNplay: function(e) {
                // Check if the current song is in the playlist already
                // If it is, then just play, otherwise add and play
                if (this.userplaylist.where({
                    ID: this.model.get('ID')
                }).length > 0) {

                    // Get the object id position
                    var objectPos = window.uzPlaylist.playlist.map(function(x) {
                        return x.id;
                    }).indexOf(this.model.get('ID'));

                    window.uzPlaylist.play(objectPos);
                } else {
                    this.userplaylist.add(this.model);
                    this.userplaylist.save();
                    window.uzPlaylist.play(window.uzPlaylist.playlist.indexOf({
                        'id': this.model.get('ID')
                    }));
                }
                e.preventDefault();
                e.stopPropagation();
            }
        });

        return songView;
    });