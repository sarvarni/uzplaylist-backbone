define(['jquery', 'underscore', 'backbone', 'modal', 'requirejstext!templates/sidenav.html', 'requirejstext!locale/en_us/header.json'],
    function($, _, Backbone, modal, template, content) {
        'use strict';

        var sideView = Backbone.View.extend({

            el: '#side-navbar',

            initialize: function(options) {
                this.route = (options !== undefined) ? options.route : '/';
                this.userplaylist = (options !== undefined) ? options.userplaylist : '';

                this.listenTo(this.userplaylist, 'add', this.render);
                this.listenTo(this.userplaylist, 'remove', this.render);

                this.render();
            },

            events: {
                'click #contactLink': 'contactForm'
            },

            render: function() {
                this.template = _.template(template, {
                    content: JSON.parse(content),
                    route: this.route,
                    numPlaylist: this.userplaylist.length
                });

                this.$el.html(this.template);

                $('#contactModal').modal({
                    show: false
                });

                $('#contactModal').on('shown.bs.modal', function() {
                    $('#contactModal #sender-email').focus();
                });

                // Maintain chainability
                return this;
            },

            contactForm: function(e) {
                var self = this;

                $('#contactModal').modal('show');

                $('#contactModal #submitContact').off('click').on('click', function() {
                    var container = $('#contactModal'),
                        emailInput = $('#contactModal #sender-email'),
                        msgInput = $('#contactModal #message-text');

                    if (self.validateEmail(emailInput.val())) {
                        emailInput.parent().removeClass('has-error');
                        emailInput.next('.form-control-feedback').addClass('hidden');
                    } else {
                        emailInput.parent().addClass('has-error');
                        emailInput.next('.form-control-feedback').removeClass('hidden');
                    }

                    if (msgInput.val() !== '' && msgInput.val().length > 0) {
                        msgInput.parent().removeClass('has-error');
                        msgInput.next('.form-control-feedback').addClass('hidden');
                    } else {
                        msgInput.parent().addClass('has-error');
                        msgInput.next('.form-control-feedback').removeClass('hidden');
                    }

                    if (container.find('.has-error').length > 0) {
                        container.find('.feedback-error').removeClass('hidden').focus();
                    } else {
                        container.find('.feedback-error').addClass('hidden');

                        // Submit to api
                        $.ajax({
                            type: 'POST',
                            url: 'api/v1.0/contact',
                            data: JSON.stringify({
                                email: emailInput.val(),
                                msg: msgInput.val()
                            }),
                            contentType: 'application/json; charset=utf-8',
                            dataType: 'json'
                        }).done(function(data) {
                            if (data.success !== undefined) {

                                // Success
                                container.find('.feedback-success').removeClass('hidden');
                                emailInput.val('');
                                msgInput.val('');

                                setTimeout(function() {
                                    container.find('.feedback-success').addClass('hidden');
                                }, 3000);

                                setTimeout(function() {
                                    $('#contactModal').modal('hide');
                                }, 4000);
                            } else {
                                // Some kind of error
                                alert('Some kind of error that should not have happened - happened. Please email us directly at support@uzplaylist.com.');
                            }
                        }).fail(function(jqXHR, textStatus) {
                            console.log('error ' + textStatus);
                        });
                    }
                });

                e.preventDefault();
            },

            validateEmail: function(email) {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            },

            resetWithOptions: function(options) {
                this.route = (options !== undefined) ? options.route : '/';
                this.userplaylist = (options !== undefined) ? options.userplaylist : '';

                this.render();
            }
        });

        return sideView;
    });