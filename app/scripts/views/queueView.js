define(['jquery', 'underscore', 'backbone', 'requirejstext!templates/queue.html', 'views/songView'],
    function($, _, Backbone, template, songView) {
        'use strict';

        var queueView = Backbone.View.extend({

            initialize: function(options) {
                this.userplaylist = options.userplaylist;
                this.collectionSongs = options.collectionSongs;

                this.listenTo(this.userplaylist, 'remove', this.render);
            },

            events: {
                'keyup #search': 'search',
                'click #btn-remove-all': 'removeFromPlaylist'
            },

            render: function() {
                this.template = _.template(template);

                this.$el.html(this.template);

                if (this.userplaylist.length > 0) {
                    this.renderList(this.userplaylist);
                    this.$el.find('#btn-remove-all').prop('disabled', false);
                } else {
                    this.$el.find('#playlist').html('There are no songs in your playlist. Please go to <a href="#">Discover</a> and add songs to your playlist.');
                    this.$el.find('#btn-remove-all').prop('disabled', true);
                }

                // Maintain chainability
                return this;
            },

            renderList: function(item) {
                var self = this;
                this.$el.find('#playlist').html('');
                _.each(item.toArray(), function(song) {
                    self.$el.find('#playlist').append((new songView({
                        model: song,
                        userplaylist: self.userplaylist
                    })).render().$el);
                });
            },

            removeFromPlaylist: function() {
                var self = this;

                _.each(this.userplaylist.toArray(), function(model) {
                    if (self.userplaylist.where({
                        'ID': model.get('ID')
                    }).length > 0) {

                        self.userplaylist.remove(model);
                        self.userplaylist.save();
                    }
                });
            }
        });

        return queueView;
    });