require.config({
    paths: {
        jquery: '../bower_components/jquery/dist/jquery',
        backbone: '../bower_components/backbone/backbone',
        modal: '../bower_components/bootstrap-sass-official/vendor/assets/javascripts/bootstrap/modal',
        alert: '../bower_components/bootstrap-sass-official/vendor/assets/javascripts/bootstrap/alert',
        underscore: '../bower_components/underscore/underscore',
        requirejstext: '../bower_components/requirejs-text/text',
        modernizr: '../bower_components/modernizr/modernizr',
        jplayer: '../bower_components/jplayer/dist/jplayer/jquery.jplayer',
        jplaylist: '../bower_components/jplayer/dist/add-on/jplayer.playlist',
        dropzone: '../bower_components/dropzone/downloads/dropzone-amd-module',
        sidr: '../bower_components/sidr/jquery.sidr.min',
        fastclick: '../bower_components/fastclick/lib/fastclick',
        googleanalytics: 'ga/analytics'
    },
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        },
        sidr: {
            deps: [
                'jquery'
            ],
            exports: 'sidr'
        },
        modal: {
            deps: ['jquery']
        },
        alert: {
        	deps: ['jquery']
        },
        jplayer: {
            deps: ['jquery'],
            exports: 'jplayer'
        },
        jplaylist: {
            deps: ['jquery', 'jplayer'],
            exports: 'jplaylist'
        },
        googleanalytics: {
            exports: 'ga'
        }
    }
});