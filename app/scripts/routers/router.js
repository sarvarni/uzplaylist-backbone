define(['jquery', 'backbone', 'views/ResultView', 'views/queueView', 'views/uploadView', 'views/sideView', 'googleanalytics'],
    function($, Backbone, ResultView, QueueView, UploadView, sideView, ga) {
        'use strict';

        var Router = Backbone.Router.extend({


            initialize: function(options) {
                this.userplaylist = options.userplaylist;
                this.collectionSongs = options.collectionSongs;

                var self = this;

                var sideview = new sideView({
                    userplaylist: self.userplaylist
                });

                this.on('route', function(route) {
                    sideview.resetWithOptions({
                        route: route,
                        userplaylist: self.userplaylist
                    });

                    //Scroll to top on route change
                    $('html, body').animate({
                        scrollTop: 0
                    }, 300);

                    // Send google analytics
                    if (document.location.hostname === "www.uzplaylist.com") {
                        self.gaPageView();
                    }
                });

                // Start watching for hash changes
                Backbone.history.start();
            },

            routes: {
                '': 'home',
                'queue': 'playlist',
                'upload': 'upload',
                '*notFound': 'notFound'
                //"artist/:query": "artistProfile"
            },

            home: function() {
                var resultView = new ResultView({
                    userplaylist: this.userplaylist,
                    collectionSongs: this.collectionSongs
                });
                $('#content-area').html(resultView.render().$el);
            },

            playlist: function() {
                var queueView = new QueueView({
                    userplaylist: this.userplaylist,
                    collectionSongs: this.collectionSongs
                });
                $('#content-area').html(queueView.render().$el);
            },

            /*artistProfile: function(query) {
                var profileView = new artistProfileView();
                profileView.render({
                    query: query
                });
            },*/

            upload: function() {
                var uploadView = new UploadView({
                    collectionSongs: this.collectionSongs
                });
                $('#content-area').html(uploadView.render().$el);
            },

            notFound: function() {
                /*jshint multistr: true */
                $('#content-area').html('\
            		<div class="header-controls col-xs-12 col-sm-9 col-md-10"> \
						<div class="row"> \
							<div class="col-sm-12"> \
								<h1>Page not found</h1> \
							</div> \
						</div> \
					</div> \
					<h1>404 - Page Not Found.</h1><p>The page you are looking for does not exist. Sorry for the inconvenience.</p>');
            },

            gaPageView: function() {
                var url = Backbone.history.getFragment();

                if (!/^\//.test(url)) url = '/' + url;

                console.log(url);
                ga('send', {
                    'hitType': 'pageview',
                    'page': url
                });
            }
        });

        return Router;

    });