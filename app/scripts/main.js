/*global require*/
require(['config/config'], function() {
    'use strict';

    require(['jquery', 'underscore', 'backbone', 'modernizr', 'routers/router', 'views/PlayerView', 'collections/userPlaylist', 'collections/collectionSongs', 'sidr', 'fastclick', 'googleanalytics'],
        function($, _, Backbone, modernizr, Router, PlayerView, userPlaylist, collectionSongs, sidr, fastclick, ga) {

            var playlist = new userPlaylist();
            collectionSongs = new collectionSongs();

            collectionSongs.fetch({
                error: function() {
                    console.log('Cannot retrive songs from server');
                }
            });

            new Router({
                userplaylist: playlist,
                collectionSongs: collectionSongs
            });

            new PlayerView({
                userplaylist: playlist,
                collectionSongs: collectionSongs
            });

            $('#btn-navigation').sidr({
                name: 'sidr-main',
                source: '#side-navbar',
                onOpen: function() {
                    $('a').on('click', function(e) {

                        if (e.currentTarget.id === 'sidr-id-contactLink') {
                            $('#contactLink').trigger('click');
                            e.preventDefault();
                        } else {
                            $(this).parent('li').siblings().removeClass('sidr-class-active');
                            $(this).parent('li').addClass('sidr-class-active');
                        }

                        $.sidr('close', 'sidr-main');
                    });
                }
            });

            // Fast Click
            fastclick.attach(document.body);

            // Responsive, close on window resize
            $(window).resize(_.debounce(function() {
                $.sidr('close', 'sidr-main');
            }, 500));

            // Load google analytics
            if (document.location.hostname === "www.uzplaylist.com") {
                ga('create', 'UA-50508402-4', 'none');
                ga('send', 'pageview');
            } else {
                console.log('Analytics disabled.');
            }
        });
});