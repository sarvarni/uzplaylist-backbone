<?php
	//next example will recieve all messages for specific conversation
	$service_url = 'http://www.uzplaylist.com/api/v1.0/song';
	$curl = curl_init($service_url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$curl_response = curl_exec($curl);
	if ($curl_response === false) {
	    $info = curl_getinfo($curl);
	    curl_close($curl);
	    die('error occured during curl exec. Additioanl info: ' . var_export($info));
	}
	curl_close($curl);
	$decoded = json_decode($curl_response);
	if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
	    die('error occured: ' . $decoded->response->errormessage);
	}
?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>UzPlaylist - Stream Uzbek Music</title>
    <meta name="description" content="Playlist of Uzbek hits including Shahzoda, Rayhon and more. Stream uzbek mp3 online for free by creating your own playlist with easy to use user interface.">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0">
    <meta property="og:site_name" content="UzPlaylist - Stream Uzbek Music">
    <meta property="og:description" content="Playlist of Uzbek hits including Shahzoda, Rayhon and more. Stream uzbek mp3 online for free by creating your own playlist with easy to use user interface.">
    <meta property="og:locale" content="en_US">  
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="stylesheet" href="../styles/main.css">
</head>

<body>
    <!--[if lt IE 10]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <div class="container-fluid content-wrapper">
    	<div class="header-mobile visible-xs">
	    	<h1 class="text-center h3">
	    		<a class="brand" href="#">UzPlaylist</a>
	    	</h1>
	    </div>
        <div id="side-navbar" class="hidden-xs col-sm-3 col-md-2">
        	<a class="brand hidden-xs" href="#">UzPlaylist</a>

			<ul id="navigation" class="list-unstyled">
			    <li class="active">
			        <a href="../#"><i class="fa fa-search" aria-hidden="true"></i> Discover <span class="sr-only">new uzbek music</span></a>
			    </li>
			    <li>
			        <a href="../#upload"><i class="fa fa-upload" aria-hidden="true"></i> Upload</a>
			    </li>
			    <li>
			        <a href="../#queue"><i class="fa fa-indent" aria-hidden="true"></i> My Playlist</a>
			    </li>
			</ul>
        </div>
        <div id="content-area" class="main-content-wrapper col-xs-12 col-sm-9 col-md-10 col-md-offset-2 col-sm-offset-3">
        	<div class="header-controls col-xs-12 col-sm-9 col-md-10">
				<div class="row">
					<div class="col-xs-4">
						<h1>Discover <span class="sr-only">and stream new uzbek music</span></h1>
					</div>
					<div class="col-xs-4">
						<div id="form-search" class="form-group" method="get">
						    <label for="search" class="sr-only">Search Uzplaylist</label>
						    <input type="search" class="form-control" autocomplete="off" name="q" id="search" placeholder="Search...">
						</div>
					</div>
					<div class="col-xs-4">
						<button id="btn-play-all" class="btn btn-default">Play All</button>
					</div>
				</div>
			</div>

			<ul id="results" class="list-unstyled list-group">
				<?php

					foreach ($decoded as $key => $object) {
					    //echo $object->title;

					    echo '<li data-songid="' . $object->ID . '" data-filename="' . $object->fileName . '" class="list-group-item">
						    <div class="row">
								<div class="col-xs-3 col-sm-1">
									<a href="javascript:void()" data-play="">
										<img src="../images/cover64.png" alt="...">
									</a>
								</div>
								<div class="col-xs-7 col-sm-10">
									<a href="javascript:void()" data-play="">
										<div class="list-group-item-content">
											<h2 class="title"><span class="sr-only">Download ' . $object->artist . ' -</span> ' . $object->title . '<span class="sr-only">.mp3</span></h2>
											<p class="byline">' . $object->artist . '</p>
										</div>
									</a>
								</div>
								<div class="col-xs-2 col-sm-1">
									<a id="btn-queue" href="javascript:void" title="Add to Queue">
										<i class="icon fa fa-plus-circle fa-2x" aria-hidden="true"></i>
									</a>
								</div>
							</div>	
						</li>';
					}
				?>
			</ul>

        </div>
    </div>
</body>
</html>
